const { Sequelize } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id : {
          type : Sequelize.INTEGER,
          allowNull : false,
          index : true,
          unique: true,
          primaryKey: true,
          autoIncrement: true,
        },
        firstName: { type: Sequelize.STRING, allowNull: false },
        lastName: { type: Sequelize.STRING, allowNull: false },
        userName: { type: Sequelize.STRING, allowNull: false },
        hash: { type: Sequelize.STRING, allowNull: false },
        email: { type: Sequelize.STRING, allowNull: false },
        phone: { type: Sequelize.BIGINT, allowNull: false },
        userCode: { type: Sequelize.STRING, allowNull: false },
        password: { type: Sequelize.STRING, allowNull: false },
        userType: { type: Sequelize.INTEGER, allowNull: false },
        /*masterId: { type: Sequelize.INTEGER, allowNull: false },
        roleId: { type: Sequelize.INTEGER, allowNull: false },*/
        status: { type: Sequelize.INTEGER, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true },
        createdAt: { type: Sequelize.DATEONLY, allowNull: true }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
            attributes: { exclude: ['hash'] }
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        }
    };

    return sequelize.define('user', attributes, options);
}
﻿const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('config/database');

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    loginUser,
    delete: _delete
};

async function authenticate({ email, password }) {
    const userSelector = {
       /* include: [
            { model: db.userPermission, as: 'user_permission', required: false, attributes:['permissions']}
        ],*/
        where: [{ 'email': email }],
    }

    const selector = Object.assign({}, userSelector);
   /* db.user.hasOne(db.userPermission, {foreignKey: 'userId'})
    db.userPermission.belongsTo(db.user, {foreignKey: 'userId'});*/

    const user = await db.user.scope('withHash').findOne(selector);

    console.log(config);

    if (!user || !(await bcrypt.compare(password, user.hash)))
        throw 'Username or password is incorrect';

    // authentication successful
    const token = jwt.sign({ sub: user.id }, config.jwt.secret_key, { expiresIn: '1h' });
    return { ...omitHash(user.get()), token };
}

async function loginUser(token) {
    return await db.user.findOne({ where: { hash: token } });
}

async function getAll() {
    return await db.user.findAll();
}

async function getById(id) {
    return await getUser(id);
}

async function create(params) {
    // validate
    if (await db.user.findOne({ where: { userName: params.userName } })) {
        throw 'Username "' + params.userName + '" is already taken';
    }

    // hash password
    if (params.password) {
        params.hash = await bcrypt.hash(params.password, 10);
    }

    // save user
    await db.user.create(params);
}

async function update(id, params) {
    const user = await getUser(id);

    // validate
    const usernameChanged = params.userName && user.userName !== params.userName;
    if (usernameChanged && await db.user.findOne({ where: { userName: params.userName } })) {
        throw 'Username "' + params.userName + '" is already taken';
    }

    // hash password if it was entered
    if (params.password) {
        params.hash = await bcrypt.hash(params.password, 10);
    }

    // copy params to user and save
    Object.assign(user, params);
    await user.save();

    return omitHash(user.get());
}

async function _delete(id) {
    const user = await getUser(id);
    await user.destroy();
}

// helper functions

async function getUser(id) {
    const user = await db.user.findByPk(id);
    if (!user) throw 'user not found';
    return user;
}

function omitHash(user) {
    const { hash, ...userWithoutHash } = user;
    return userWithoutHash;
}
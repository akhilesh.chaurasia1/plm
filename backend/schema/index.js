var Joi = require('joi');

export const validate = function (obj, options = { allowUnknown: true }) {
  return function(req, res, next) {
    const validations = [];
    Object.keys(obj).forEach(key => {
      validations.push(Joi.validate(req[key], obj[key], options));
    });
    Promise.all(validations).
      then(response => next()).
      catch(err => next(err));
  }
}
